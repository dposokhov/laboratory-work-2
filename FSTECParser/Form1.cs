﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Windows.Forms;
using OfficeOpenXml;

namespace FSTECParser
{
    public partial class Form1 : Form
    {
        public BindingList<Threat> threatList = new BindingList<Threat>();
        public BindingList<Threat> threatListForView = new BindingList<Threat>();
        BindingList<Threat> checkList = new BindingList<Threat>();
        public int CountOfPage = 1;
        public Form1()
        {
            InitializeComponent();
            this.listBox1.DataSource = threatListForView;
            CheckDataBase();
        }

        
        private void threatParse2 (string fileName, bool isNoUpdate = true, bool postUpdate = false)
        {
            using (ExcelPackage xlPackage = new ExcelPackage(new FileInfo(fileName)))
            {
                var myWorksheet = xlPackage.Workbook.Worksheets.First();
                var totalRows = myWorksheet.Dimension.End.Row;
                var totalColumns = myWorksheet.Dimension.End.Column;
                if (postUpdate)
                {
                    threatList.Clear();
                }
                for (int rowNum = 3; rowNum <= totalRows; rowNum++)
                {
                    var row = myWorksheet.Cells[rowNum, 1, rowNum, totalColumns].Select(c => c.Value == null ? string.Empty : c.Value.ToString());
                    
                    Threat currentThreat = MakeThreat(row.ToArray<string>());
                    if (isNoUpdate)
                    {
                        threatList.Add(currentThreat);
                    }
                    else
                    {
                        checkList.Add(currentThreat);
                    }
                }
            }
        }

        private void ntbParse2_Click(object sender, EventArgs e)
        {
            
        }
        private Threat MakeThreat(string[] temp)
        {

            for (int i = 0; i < 7; i++)
            {
                if (temp[i].Contains("_x000d_"))
                {
                    temp[i] = temp[i].Replace("_x000d_", "\r");
                }
            }
                Threat threat = new Threat(Int32.Parse(temp[0]), temp[1], temp[2], temp[3], temp[4], StrToBool(temp[5]), StrToBool(temp[6]), StrToBool(temp[7]));
                return threat;
        }
        private bool StrToBool(string s)
        {
            if (s=="1" || s=="True")
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        private void CheckDataBase()
        {
            if (File.Exists(AppDomain.CurrentDomain.BaseDirectory + "\\ThreatListStec.xlsx"))
            {
                threatParse2(AppDomain.CurrentDomain.BaseDirectory + "\\ThreatListStec.xlsx");
                FillViewList();
            }
            else
            {
                MessageBox.Show("Скачайте файл из интернета и распарсите его", "Подсказка при первом запуске");
            }
            

        }
        
        private void FillViewList()
        {
            int stopNumber;
            threatListForView.Clear();
            if (CountOfPage * 15 - 1 > threatList.Count - 1)
                stopNumber = threatList.Count;
            else
                stopNumber = CountOfPage * 15;
            for (int i = (CountOfPage - 1) * 15; i < stopNumber ; i++)
            {
                threatListForView.Add(threatList[i]);
            }
            textBoxNumber.Text = CountOfPage.ToString();

        }


        private void ParseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            threatParse2(AppDomain.CurrentDomain.BaseDirectory + "\\ThreatListStec.xlsx");
            FillViewList();
        }


        private void buttonNext_Click(object sender, EventArgs e)
        {
            int maxCount;
            if (threatList.Count % 15 != 0)
                maxCount = threatList.Count / 15 + 1;
            else
                maxCount = threatList.Count / 15;
            if (CountOfPage >= maxCount)
            {

            }
            else
            {
                CountOfPage++;
                FillViewList();
            }
        }

        private void buttonUndo_Click(object sender, EventArgs e)
        {
            if (CountOfPage == 1)
            {
            }
            else
            {
                CountOfPage--;
                FillViewList();
            }
        }

        private void ShowFullInfo(object sender, MouseEventArgs e)
        {
            Threat current = (Threat)listBox1.SelectedItem;
            MessageBox.Show(current.FullInfo(), "Полная информация об угрозе");
        }

        private void скачатьФайлExсelИзИнтернетаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
            if (!File.Exists(AppDomain.CurrentDomain.BaseDirectory + "\\ThreatListStec.xlsx"))
            {
                new WebClient().DownloadFile("https://bdu.fstec.ru/documents/files/thrlist.xlsx", AppDomain.CurrentDomain.BaseDirectory + "\\ThreatListStec.xlsx");
                MessageBox.Show("Готово", "Загрузка");
            }
            
            else { MessageBox.Show("Всё уже скачано"); }

            
        }

        private void проверитьОбновлениеToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (!File.Exists(AppDomain.CurrentDomain.BaseDirectory + "\\ThreatListStec.xlsx"))
            {
                MessageBox.Show("Не существует базы данных на диске. Скачайте и распарсите файл xlsx", "Ошибка");
            }
            else
            {
                new WebClient().DownloadFile("https://bdu.fstec.ru/documents/files/thrlist.xlsx", AppDomain.CurrentDomain.BaseDirectory + "\\New.xlsx");
                threatParse2(AppDomain.CurrentDomain.BaseDirectory + "\\New.xlsx", false);
                string text = "";
                int CountOfDiff = 0;
                List<int> listOfDiff = new List<int>();

                if (threatList.Count == checkList.Count)
                {
                    for (int i = 0; i < threatList.Count; i++)
                    {
                        Threat oldT = threatList[i];
                        Threat newT = checkList[i];
                        if (!oldT.Equals(newT))
                        {
                            CountOfDiff++;
                            listOfDiff.Add(i);
                            text += $"\nБЫЛО\n{oldT.FullInfo()}\n" +
                                $"СТАЛО\n{newT.FullInfo()}";
                        }
                    }
                    
                }
                else
                {
                    int minCount = Int32.MaxValue;
                    int maxCount = Int32.MaxValue;

                    if (threatList.Count < checkList.Count)
                    {
                        minCount = threatList.Count;
                        maxCount = checkList.Count;

                    }
                    else
                    {
                        minCount = checkList.Count;
                        maxCount = threatList.Count;
                    }
                    for (int i = 0; i < minCount; i++)
                    {
                        Threat oldT = threatList[i];
                        Threat newT = checkList[i];
                        if (!oldT.Equals(newT))
                        {
                            CountOfDiff++;
                            listOfDiff.Add(i);
                        }
                    }
                    for (int i = minCount; i < maxCount-1; i++)
                    {
                        CountOfDiff++;
                        listOfDiff.Add(i);
                    }
                }
                string output = $"Обновление прошло успешно.\n\n" +
                    $"Количество обновленных записей: {CountOfDiff}\n" +
                    $"Индексы обновленных записей: { ShowItems(listOfDiff)}"
                 + text;
                
                
                File.Delete(AppDomain.CurrentDomain.BaseDirectory + "\\ThreatListStec.xlsx");
                File.Move(AppDomain.CurrentDomain.BaseDirectory + "\\New.xlsx", AppDomain.CurrentDomain.BaseDirectory + "\\ThreatListStec.xlsx");
                threatParse2(AppDomain.CurrentDomain.BaseDirectory + "\\ThreatListStec.xlsx",true,true);
                FillViewList();
                MessageBox.Show($"Обновление прошло успешно \n" +
                    $"Количество обновленных записей: {CountOfDiff} \n" +
                    $"Индексы обновленных записей : {ShowItems(listOfDiff)} \n" +
                    $"Для просмотра полной информации откройте файл AboutUpdate.txt", "Краткая информация");
                using (StreamWriter sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\AboutUpdate.txt"))
                {
                    sw.Write(output);
                }
            }
            

        }
        private string ShowItems(List<int> ints)
        {
            string s = "";
            foreach (var item in ints)
            {
                s += (item+1).ToString() + " ";
            }
            return s;
        }
    }
}
