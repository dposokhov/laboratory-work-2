﻿namespace FSTECParser
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.buttonNext = new System.Windows.Forms.Button();
            this.buttonUndo = new System.Windows.Forms.Button();
            this.textBoxNumber = new System.Windows.Forms.TextBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.базаДанныхToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.скачатьФайлExсelИзИнтернетаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.проверитьОбновлениеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.базаДанныхToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.проверитьОбновлениеToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // listBox1
            // 
            this.listBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.listBox1.FormattingEnabled = true;
            this.listBox1.HorizontalScrollbar = true;
            this.listBox1.ItemHeight = 20;
            this.listBox1.Location = new System.Drawing.Point(12, 27);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(802, 324);
            this.listBox1.TabIndex = 4;
            this.listBox1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.ShowFullInfo);
            // 
            // buttonNext
            // 
            this.buttonNext.Location = new System.Drawing.Point(475, 355);
            this.buttonNext.Name = "buttonNext";
            this.buttonNext.Size = new System.Drawing.Size(75, 23);
            this.buttonNext.TabIndex = 6;
            this.buttonNext.Text = ">";
            this.buttonNext.UseMnemonic = false;
            this.buttonNext.UseVisualStyleBackColor = true;
            this.buttonNext.Click += new System.EventHandler(this.buttonNext_Click);
            // 
            // buttonUndo
            // 
            this.buttonUndo.Location = new System.Drawing.Point(351, 355);
            this.buttonUndo.Name = "buttonUndo";
            this.buttonUndo.Size = new System.Drawing.Size(75, 23);
            this.buttonUndo.TabIndex = 7;
            this.buttonUndo.Text = "<";
            this.buttonUndo.UseVisualStyleBackColor = true;
            this.buttonUndo.Click += new System.EventHandler(this.buttonUndo_Click);
            // 
            // textBoxNumber
            // 
            this.textBoxNumber.Location = new System.Drawing.Point(433, 357);
            this.textBoxNumber.Name = "textBoxNumber";
            this.textBoxNumber.ReadOnly = true;
            this.textBoxNumber.Size = new System.Drawing.Size(36, 20);
            this.textBoxNumber.TabIndex = 8;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.базаДанныхToolStripMenuItem,
            this.базаДанныхToolStripMenuItem1});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(821, 24);
            this.menuStrip1.TabIndex = 9;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // базаДанныхToolStripMenuItem
            // 
            this.базаДанныхToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.скачатьФайлExсelИзИнтернетаToolStripMenuItem,
            this.проверитьОбновлениеToolStripMenuItem});
            this.базаДанныхToolStripMenuItem.Name = "базаДанныхToolStripMenuItem";
            this.базаДанныхToolStripMenuItem.Size = new System.Drawing.Size(45, 20);
            this.базаДанныхToolStripMenuItem.Text = "Файл";
            // 
            // скачатьФайлExсelИзИнтернетаToolStripMenuItem
            // 
            this.скачатьФайлExсelИзИнтернетаToolStripMenuItem.Name = "скачатьФайлExсelИзИнтернетаToolStripMenuItem";
            this.скачатьФайлExсelИзИнтернетаToolStripMenuItem.Size = new System.Drawing.Size(194, 22);
            this.скачатьФайлExсelИзИнтернетаToolStripMenuItem.Text = "Скачать из интернета ";
            this.скачатьФайлExсelИзИнтернетаToolStripMenuItem.Click += new System.EventHandler(this.скачатьФайлExсelИзИнтернетаToolStripMenuItem_Click);
            // 
            // проверитьОбновлениеToolStripMenuItem
            // 
            this.проверитьОбновлениеToolStripMenuItem.Name = "проверитьОбновлениеToolStripMenuItem";
            this.проверитьОбновлениеToolStripMenuItem.Size = new System.Drawing.Size(194, 22);
            this.проверитьОбновлениеToolStripMenuItem.Text = "Распарсить";
            this.проверитьОбновлениеToolStripMenuItem.Click += new System.EventHandler(this.ParseToolStripMenuItem_Click);
            // 
            // базаДанныхToolStripMenuItem1
            // 
            this.базаДанныхToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.проверитьОбновлениеToolStripMenuItem1});
            this.базаДанныхToolStripMenuItem1.Name = "базаДанныхToolStripMenuItem1";
            this.базаДанныхToolStripMenuItem1.Size = new System.Drawing.Size(84, 20);
            this.базаДанныхToolStripMenuItem1.Text = "База данных";
            // 
            // проверитьОбновлениеToolStripMenuItem1
            // 
            this.проверитьОбновлениеToolStripMenuItem1.Name = "проверитьОбновлениеToolStripMenuItem1";
            this.проверитьОбновлениеToolStripMenuItem1.Size = new System.Drawing.Size(202, 22);
            this.проверитьОбновлениеToolStripMenuItem1.Text = "Проверить обновление";
            this.проверитьОбновлениеToolStripMenuItem1.Click += new System.EventHandler(this.проверитьОбновлениеToolStripMenuItem1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(821, 382);
            this.Controls.Add(this.textBoxNumber);
            this.Controls.Add(this.buttonUndo);
            this.Controls.Add(this.buttonNext);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Button buttonNext;
        private System.Windows.Forms.Button buttonUndo;
        private System.Windows.Forms.TextBox textBoxNumber;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem базаДанныхToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem проверитьОбновлениеToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem скачатьФайлExсelИзИнтернетаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem базаДанныхToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem проверитьОбновлениеToolStripMenuItem1;
    }
}

